from django.contrib import admin
from imdb.models import WatchList, StreamPlatform

admin.site.register(WatchList),
admin.site.register(StreamPlatform)
