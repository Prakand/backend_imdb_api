from rest_framework import serializers
from imdb.models import WatchList, StreamPlatform


class WatchListSerializer(serializers.ModelSerializer):
    class Meta:
        model = WatchList
        fields = "__all__"

        # object level validations

    def validate(self, data):
        if data['name'] == data['description']:
            raise serializers.ValidationError("Tittle & Description Should be Different")
        else:
            return data

        # field level validations

    def validate_name(self, value):
        if len(value) < 2:
            raise serializers.ValidationError("Name is too short !!")
        else:
            return value


class StreamPlatformSerializer(serializers.ModelSerializer):
    watchlist = WatchListSerializer(many=True, read_only=True)

    class Meta:
        model = StreamPlatform
        fields = "__all__"


# def name_len(value):
#     if len(value) < 2:
#         raise serializers.ValidationError("Name is too shorts")
#
#
# class MovieSerializer(serializers.Serializer):
#     id = serializers.IntegerField(read_only=True)
#     name = serializers.CharField(validators=[name_len])
#     description = serializers.CharField()
#     active = serializers.BooleanField()
#
#     def create(self, validated_data):
#         return Movie.objects.create(**validated_data)
#
#     def update(self, instance, validated_data):
#         instance.name = validated_data.get('name', instance.name)
#         instance.description = validated_data.get('description', instance.description)
#         instance.active = validated_data.get('active', instance.active)
#         instance.save()
#         return instance
#
#
