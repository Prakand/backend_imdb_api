from django.urls import path
from imdb.views import WatchListAV, WatchDetailsAV, StreamPlatformAV

# from imdb.views import movie_list, movie_details

urlpatterns = [
    path('list/', WatchListAV.as_view(), name='movie-list'),
    path('list/<int:pk>/', WatchDetailsAV.as_view(), name='movie-details'),
    path('stream/', StreamPlatformAV.as_view(), name='stream-platform'),

    # path('list/', movie_list, name='movie-list'),
    # path('list/<int:pk>/', movie_details, name='movie-details')
]
