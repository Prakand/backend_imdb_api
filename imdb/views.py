from rest_framework.decorators import api_view
from rest_framework.response import Response
from imdb.models import WatchList, StreamPlatform
from imdb.serializers import WatchListSerializer, StreamPlatformSerializer
from rest_framework import status
from rest_framework.views import APIView


# class based views


class StreamPlatformAV(APIView):
    def get(self, request):
        platform = StreamPlatform.objects.all()
        serializer = StreamPlatformSerializer(platform, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        serializer = StreamPlatformSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class WatchListAV(APIView):

    def get(self, request):
        movies = WatchList.objects.all()
        serializer = WatchListSerializer(movies, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        serializer = WatchListSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)


class WatchDetailsAV(APIView):

    def get(self, request, pk):
        movie = WatchList.objects.get(pk=pk)
        serializer = WatchListSerializer(movie)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        movie = WatchList.objects.get(pk=pk)
        serializer = WatchListSerializer(movie, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        try:
            movie = WatchList.objects.get(pk=pk)
            movie.delete()
            return Response({"Message": "Successfully Deleted"}, status=status.HTTP_204_NO_CONTENT)
        except WatchList.DoesNotExist:
            return Response({"Error": f"Movie with id {pk} doesn't Exist"}, status=status.HTTP_404_NOT_FOUND)

# function based views

# @api_view(['GET', 'POST'])
# def movie_list(request):
#     if request.method == 'GET':
#         movies = Movie.objects.all()
#         serializer = MovieSerializer(movies, many=True)
#         return Response(serializer.data, status=status.HTTP_200_OK)
#     if request.method == 'POST':
#         serializer = MovieSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         else:
#             return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#
#
# @api_view(['GET', 'PUT', 'DELETE'])
# def movie_details(request, pk):
#     if request.method == 'GET':
#
#         try:
#             movie = Movie.objects.get(pk=pk)
#         except Movie.DoesNotExist:
#             return Response({"Error": "This movie does not exist"}, status=status.HTTP_404_NOT_FOUND)
#
#     serializer = MovieSerializer(movie)
#     return Response(serializer.data, status=status.HTTP_200_OK)
#
# if request.method == 'PUT':
#     serializer = MovieSerializer(data=request.data)
#     if serializer.is_valid():
#         serializer.save()
#         return Response(serializer.data, status=status.HTTP_201_CREATED)
#     else:
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#
# if request.method == "DELETE":
#     try:
#         movie = Movie.objects.get(pk=pk)
#         movie.delete()
#         return Response({"message": f"Successfully Deleted item {pk}"}, status=status.HTTP_204_NO_CONTENT)
#     except Movie.DoesNotExist:
#         return Response({"message": f"Item with ID {pk} not exist"}, status=status.HTTP_404_NOT_FOUND)

#
# def movie_list(request):
#     movies = Movie.objects.all()
#     movie_data = []
#     for movie in movies:
#         movie_data.append({
#             "name": movie.name,
#             "description": movie.description,
#             "active": movie.active
#         })
#
#     data = {
#         "movies": movie_data
#     }
#     return JsonResponse(data)
#

# def movie_details(request, pk):
#     movie = Movie.objects.get(pk=pk)
#     data = {
#         'name': movie.name,
#         'description': movie.description,
#         'active': movie.active
#     }
#     return JsonResponse(data)
